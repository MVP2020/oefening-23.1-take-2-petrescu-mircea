﻿using System;
using System.Windows;

namespace _23._1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Random myrandom = new Random();
            myrandom.Next();
            firstlabel.Content = myrandom.Next(10001);
            secondlabel.Content = myrandom.Next(10000);
        }
    }
}
